const gulp = require("gulp"),
      rename = require("gulp-rename"),
      rollup = require("gulp-rollup-babel"),
      plumber = require("gulp-plumber"),
      uglify = require("gulp-uglify"),
      dest = require("gulp-dest"),
  //    cssnano = require("cssnano"),
      nested = require("postcss-nested"),
      livereload = require("gulp-livereload"),
      postcss = require("gulp-postcss"),
      cssnext = require("cssnext"),
      autoprefixer = require("autoprefixer-core");

gulp.task("js", () => {
  return gulp.src("data/index.es6")
    .pipe(plumber())
    .pipe(rollup({
      format: "iife",
      moduleName: "index"
    }))
    .pipe(rename("joinery_table"))
    .pipe(dest("./build", { ext: ".js" }))
    .pipe(gulp.dest("./"))
    .pipe(uglify())
    .pipe(dest("./", { ext: ".min.js" }))
    .pipe(plumber.stop())
    .pipe(gulp.dest("./"));
});

gulp.task("styles", function() {
  return gulp.src("data/index.css")
    .pipe(plumber())
    .pipe(postcss([
      cssnext(),
      nested,
      autoprefixer({
        browsers: ["IE >= 10", "last 2 versions", "Safari >= 6.1", "iOS >=7"]
      })
    ]))
    .pipe(gulp.dest("./build"))
//    .pipe(combinemq({
//      beautify: true
//    }))
    .pipe(plumber.stop())
    //.pipe(gulp.dest("./"))
    .pipe(livereload());

});


gulp.task('watch', function() {
  gulp.watch("data/*es6", ["js"]);
  gulp.watch("data/*css", ["styles"]);
});

gulp.task("default", function() {
  livereload.listen();
});